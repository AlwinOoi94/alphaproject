# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/nvidia/Documents/Alwin's Workspace/Object Tracker/src/Track.cpp" "/home/nvidia/Documents/Alwin's Workspace/Object Tracker/build/src/CMakeFiles/Track.dir/Track.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "/usr/local/include/opencv"
  "/usr/local/include/neurala"
  "/usr/include/libxml2"
  "/usr/local/cuda-8.0/include"
  "/usr/local/include/neurala/Utils"
  "/usr/local/include/neurala/Recognizer"
  "/usr/local/include/neurala/Tracker"
  "/usr/local/include/neurala/Finder"
  "/usr/local/include/neurala/Metrics"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
