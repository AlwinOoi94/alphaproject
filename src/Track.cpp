/*
 * This file is part of Neurala SDK.
 * Copyright Neurala Inc. 2013-2015. All rights reserved.
 *
 * Except as expressly permitted in the accompanying License Agreement, if at all, (a) you shall
 * not license, sell, rent, lease, transfer, assign, distribute, display, host, outsource, disclose
 * or otherwise commercially exploit or make this source code available to any third party; (b) you
 * shall not modify, make derivative works of, disassemble, reverse compile or reverse engineer any
 * part of the SDK; (c) You shall not access the SDK in order to build a similar or competitive
 * product or service; (d) no part of the this source may be copied, reproduced, distributed,
 * republished, downloaded, displayed, posted or transmitted in any form or by any means, including
 * but not limited to electronic, mechanical, photocopying, recording or other means; and (e) any
 * future release, update, or other addition to functionality of the SDK shall be subject to the
 * terms of the accompanying License Agreement. You must reproduce, on all copies made by you or
 * for you, and must not remove, alter, or obscure in any way all proprietary rights notices
 * (including copyright notices) of Neurala Inc or its suppliers on or within the copies of the
 * SDK. Any sample code provided with the SDK and designated as such are for illustrative purposes
 * only and are not to be included in your applications.
 *
 * In cases when the accompanying License Agreement permits redistribution of this file, the above
 * notice shall be reproduced its entirety in every copy of a distributed version of this file.
 */

#include <iostream>
#include <opencv2/opencv.hpp>

#include <Finder/Finder.h>
#include <Utils/VideoInput.h>
#include <Utils/VideoOutput.h>
#include <DOMParser.h>

using namespace std;
using namespace cv;
using namespace cuda;
using namespace neu;

// Initialize a neu::VideoInput class
VideoInput videoInput;


// Initialize a neu::VideoOutput to display the video
VideoOutput videoOutput;

// Create an instance of openCV's video capture and open the webcam
VideoCapture m_capture;

// Create the paths to the weights, narch and label files according to the XML specifications
 string weights;
 string narch;
 string labelPath;

void convertOpenCV(const cv::Mat& mat, cv::Mat& output)
{
	if (mat.type() % 8 != 5) // not floating point
	{
		switch (mat.channels())
		{
		case 1:
			mat.convertTo(output, CV_32FC1, 1.0 / 255.0);
			break;
		case 3:
		case 4:
			mat.convertTo(output, CV_32FC3, 1.0 / 255.0);
			break;
		default:
			throw neurala::Exception(__func__, "Unsupported number of channels in the input frame");
		}
	}
	else
	{
		if (mat.channels() == 4)
		{
			mat.convertTo(output, CV_32FC3);
		}
		else
		{
			output = mat;
		}
	}
	return;
}

// converts a cv::Mat to neurala::ColorMatrix
void convertCVMatToColorMatrix(cv::Mat& mat, neurala::ColorMatrix<float>& output)
{
	Mat converted;
	convertOpenCV(mat, converted);
	vector<Mat> channel(converted.channels());
	split(converted, channel);
	vector<float* > pData;

	for(uint8_t i = 0; i < converted.channels(); ++i)
	{
		pData.push_back(channel[i].ptr<float>(0));
	}

	output = neurala::ColorMatrix<float>(converted.cols, converted.rows, pData);
}

void init(char* argv[]){
	/**********************
	 * Obtaining Database *
	 **********************/
	// Parse the Config.xml file
	neurala::DOMParser config(argv[1]);

	// Get the contents of the XML file
	const string path = config.keyword("/Configuration/Network/@path");
	const string name = config.keyword("/Configuration/Network/@name");

	// Create the paths to the weights, narch and label files according to the XML specifications
	weights     = path + name + ".weights";
	narch       = path + name + ".narch";
	labelPath   = path + name + ".txt";


	/************************
	 * Loading Video Stream *
	 ************************/
	// Create sample video path
	const string car = "/home/nvidia/Documents/Alwin's Workspace/Object Tracker/Sample Video/Car - 2165.mp4";
	const string cow = "/home/nvidia/Documents/Alwin's Workspace/Object Tracker/Sample Video/COWS_AT_THE_GRASS.mp4";
	const string cam = "nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720,format=(string)I420, framerate=(fraction)24/1 ! nvvidconv flip-method=0 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink";
	
	//Open video input stream
	m_capture.open(cam);

	if(m_capture.isOpened()){
		cout<<"Camera initialization COMPLETED!\n";
	}
	else
	{
		cout<<"***Camera initialization Failed!***\n"
		    <<"Exitting the program..\n";
	}

	
	/***********************
	 * Checking CUDA Satus *
	 ***********************/
	int cudaEnabledDeviceCount = getCudaEnabledDeviceCount();
	DeviceInfo DI;
	int processorCount = DI.multiProcessorCount();

	cout << "\nCUDA enabled device count: "<<cudaEnabledDeviceCount<<"\n";
	cout << "number of core used: "<< processorCount<< "\n";	

	cout << "\nInitialization COMPLETED!\n";
	cout << "\nFinder Sample begin!";
	cout << "\nRuntime commands:\n\t(q)uit program\n\ttoggle (f)inding\n";

}

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		cerr << "\nMissing configuration file parameter! Program usage should be: "
				  << argv[0] << " /path/to/FinderSample.xml";

		exit(-1);
	}

	init(argv);

	try
	{
		int gpuIndex = -1; // -1 denotes use GPU if available, otherwise uses CPU

		// Construct our Finder
		neu::Finder finder(weights, narch, labelPath, "", videoInput, gpuIndex);

		// Store the original frame in these variables
		Mat sourceFrameOCV;
		neurala::ColorMatrix<float> colorMatrix;

		// Initialize variables for the call to Finder::find()
		map<string, vector<neurala::Polygon> > output;
		vector<string> labels;
		vector<neurala::Point<float> > polygonPoints;

		// Finding state variable
		bool isFinding = true;

		// Loop Through Camera Frames
		while (videoOutput.lastKey() != 'q')
		{
			// Read in the next frame from OpenCV
			m_capture.read(sourceFrameOCV);

			// Convert the cv::Mat into neurala::ColorMatrix
			convertCVMatToColorMatrix(sourceFrameOCV, colorMatrix);

			// Load the neurala::ColorMatrix into our VideoInput (to be passed to the Finder) and
			// load it into the VideoOutput (for display to the user)
			videoInput.load(colorMatrix);
			videoOutput.inputFrame(videoInput.frame());

			if (isFinding)
			{
				// Remove any results from the previous frame(s)
				output.clear();

				// In the call to find(), the Finder will attempt to find all of the object in the labels list.
				finder.find(output, labels, 0.1f);

				// For each object returned in the output, draw the object's bounding polygon and label on the videoOutput
				for (auto&& pair : output)
				{
					vector<neurala::Polygon> vector = pair.second;

					for (int i = 0; i < vector.size(); ++i)
					{
						videoOutput.drawPolygon(vector[i], 1);
						vector[i].points(polygonPoints);

						// If the Finder returned valid polygon points, add the object's label to
						// the video feed, on top of a black background (for visibility)
						if (polygonPoints.size() > 0)
						{
							neurala::Point<float> bottomRight(0.1, -0.01);
							bottomRight = polygonPoints[0] + bottomRight;

							// make sure point is greater than 0.0 less than 1.0 
							bottomRight.x = min(bottomRight.x, 1.0f);
							bottomRight.y = min(bottomRight.y, 1.0f);

							bottomRight.x = max(bottomRight.x, 0.0f);
							bottomRight.y = max(bottomRight.y, 0.0f);

							videoOutput.drawRectangle(polygonPoints[0], bottomRight, 5, neurala::EPredefinedColor::eBlack);

							videoOutput.drawText(pair.first, polygonPoints[0]);
						}
					}
				}
			}

			// Displays the image to the user
			videoOutput.showImage();

			// Handle Key Presses
			if (videoOutput.lastKey() == 'f')
			{
				// Toggle finding on and off
				isFinding = !isFinding;

				if (isFinding)
				{
					cout << "Finding turned on" << endl;
				}
				else
				{
					cout << "Finding turned off" << endl;
				}
			}
		}	

		// Destroy the videoOutput window
		videoOutput.hideImage();
	}
	catch (neurala::Exception& e)
	{
		std::cerr << "Finder Sample Caught: " << e.what() << endl;
	}
	
	return 0;
}




